//
//  MyMovie.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/17/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import Foundation


class MyMovie: NSObject, NSCoding {
    
    private var _movieImage: String!
    private var _movieTitle: String!
    private var _movieDesc: String!
    private var _movieLink: String!
    private var _movieBio: String!
    
    var movieImage: String{
        return _movieImage
    }
    var movieTitle: String {
        return _movieTitle
    }
    var movieDesc: String {
        return _movieDesc
    }
    var movieLink: String {
        return _movieLink
    }
    var movieBio: String {
        return _movieBio
    }
    
    init(imagePath: String, title: String, description: String, link: String) {
        self._movieImage = imagePath
        self._movieTitle = title
        self._movieDesc = description
        self._movieLink = link
    }
    
    override init() {
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init()
        self._movieImage = aDecoder.decodeObjectForKey("movieImage") as? String
        self._movieTitle = aDecoder.decodeObjectForKey("movieTitle") as? String
        self._movieDesc = aDecoder.decodeObjectForKey("movieDesc") as? String
        self._movieBio = aDecoder.decodeObjectForKey("movieBio") as? String
        self._movieLink = aDecoder.decodeObjectForKey("movieLink") as? String
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(self._movieImage, forKey: "movieImage")
        aCoder.encodeObject(self._movieTitle, forKey: "movieTitle")
        aCoder.encodeObject(self._movieLink, forKey: "movieLink")
        aCoder.encodeObject(self._movieBio, forKey: "movieBio")
        aCoder.encodeObject(self._movieDesc, forKey: "movieDesc")
        

    }
    
    
    
}