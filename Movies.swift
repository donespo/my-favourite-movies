//
//  Movies.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/17/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Movies: NSManagedObject {

    func saveFavMovieImg(img: UIImage) {
        
        let data = UIImagePNGRepresentation(img)
        self.movieImage = data
    }
    
    func getMovieImage() -> UIImage {
        
        let img = UIImage(data: self.movieImage!)
        return img!
    }
    
    func getObjectID() -> NSManagedObjectID {
        
        let objectId = NSManagedObjectID()
        return objectId
    }
}
