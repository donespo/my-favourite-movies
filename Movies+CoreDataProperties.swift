//
//  Movies+CoreDataProperties.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/17/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Movies {

    @NSManaged var movieTitle: String?
    @NSManaged var movieDesc: String?
    @NSManaged var movieLink: String?
    @NSManaged var movieBio: String?
    @NSManaged var movieImage: NSData?

}
