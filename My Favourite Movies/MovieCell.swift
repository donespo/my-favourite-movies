//
//  MovieCell.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/17/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieDesc: UILabel!
    @IBOutlet weak var movieLink: UILabel!
    @IBOutlet weak var movieBio: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
     
        movieImage.layer.cornerRadius = movieImage.frame.size.width / 2
        movieImage.clipsToBounds = true
        let shadow = NSShadow()
        shadow.shadowColor = UIColor.blackColor().colorWithAlphaComponent(0.18)
        shadow.shadowOffset = CGSizeMake(2.1, 2.1)
        shadow.shadowBlurRadius = 3
    
    }

    func configureMovieCell(movie: Movies) {
        
        movieTitle.text = movie.movieTitle
        movieDesc.text = movie.movieDesc
        movieLink.text = movie.movieLink
        movieImage.image = movie.getMovieImage()
    }

}
