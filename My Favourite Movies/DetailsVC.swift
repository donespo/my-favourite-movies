//
//  DetailsVC.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/18/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class DetailsVC: UIViewController {
    
    @IBOutlet weak var movieDetailTitle: UILabel!
    @IBOutlet weak var movieDetailDesc: UILabel!
    @IBOutlet weak var movieDetailLink: UILabel!
    @IBOutlet weak var movieDetailBio: UILabel!
    @IBOutlet weak var movieDetailImage: UIImageView!
    var movie: Movies!
    var movieArray = [Movies]()
    var movDetTitle: String!
    var movDetDesc: String!
    var movDetLink: String!
    var movDetBio: String!
    var movDetImg: UIImage!
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieDetailImage.layer.cornerRadius = 5.0
        movieDetailImage.clipsToBounds = true
        
        movieDetailTitle.text = movDetTitle
        movieDetailDesc.text = movDetDesc
        movieDetailLink.text = movDetLink
        movieDetailBio.text = movDetBio
        movieDetailImage.image = movDetImg
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "WebViewVC" {
            let destinationController = segue.destinationViewController as! WebViewVC
            destinationController.movieLink = movDetLink
        } else if segue.identifier == "EditMovieVC" {
            let destinationController = segue.destinationViewController as! EditMovieVC
            
            destinationController.editMovTitle = movDetTitle
            destinationController.editMovDesc = movDetDesc
            destinationController.editMovLink = movDetLink
            destinationController.editMovBio = movDetBio
            destinationController.editMovImg = movDetImg
        }
        
    }

    @IBAction func cancelAddMovieBtnPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
}
