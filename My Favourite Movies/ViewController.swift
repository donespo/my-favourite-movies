//
//  ViewController.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/17/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var movies = [Movies]()
    
//    var objectID: NSManagedObjectID!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
//        fetchAndSetResults()
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "topbar"), forBarMetrics: UIBarMetrics.Default)

        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        fetchAndSetResults()
        tableView.reloadData()
    }

    func fetchAndSetResults() {
        
        let app = UIApplication.sharedApplication().delegate as! AppDelegate
        let context = app.managedObjectContext
        let fetchRequest =  NSFetchRequest(entityName: "Movies")
        
        do {
            let results = try context.executeFetchRequest(fetchRequest)
            self.movies = results as! [Movies]
            
//            let this = try context.executeFetchRequest(fetchRequest) as! [Movies]
//            for res in this {
//                print(res.movieTitle!)
//                print(res.objectID)
//                print(res.objectID.URIRepresentation())
//            }
            
            
        } catch let err as NSError {
            print(err.debugDescription)
        }
        
        
        
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       if let cell = tableView.dequeueReusableCellWithIdentifier("MovieCell") as? MovieCell {
        cell.backgroundColor = UIColor.clearColor()
            let movie = movies[indexPath.row]
            cell.configureMovieCell(movie)
            return cell
        } else {
            return MovieCell()
        }
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 171.0
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DetailsVC" {
            let destinationController = segue.destinationViewController as! DetailsVC
            let indexPath = tableView.indexPathForSelectedRow
            
            let theMovie = movies[indexPath!.row]
            destinationController.movie = theMovie
            let movieDetailTitle = theMovie.movieTitle
            let movieDetailDesc = theMovie.movieDesc
            let movieDetailLink = theMovie.movieLink
            let movieDetailBio = theMovie.movieBio
            let movieDetailImage = theMovie.getMovieImage()
            destinationController.movDetTitle = movieDetailTitle
            destinationController.movDetBio = movieDetailBio
            destinationController.movDetLink = movieDetailLink
            destinationController.movDetDesc = movieDetailDesc
            destinationController.movDetImg = movieDetailImage
            
            

        }
    }
    

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let context = appDelegate.managedObjectContext
            
            context.deleteObject(movies[indexPath.row])
            appDelegate.saveContext()
            
            movies.removeAtIndex(indexPath.row)
            
            tableView.reloadData()
        }
    }
    
}

