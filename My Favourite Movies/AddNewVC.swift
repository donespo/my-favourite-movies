//
//  AddNewVC.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/17/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit
import CoreData
class AddNewVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var addTitleField: UITextField!
    @IBOutlet weak var addDescField: UITextField!
    @IBOutlet weak var addLinkField: UITextField!
    @IBOutlet weak var addMovieBioField: UITextView!
    @IBOutlet weak var movieImage: UIImageView!
    
    var imgPicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        movieImage.layer.cornerRadius = movieImage.bounds.width / 2
        movieImage.clipsToBounds = true
        
        addTitleField.attributedPlaceholder = NSAttributedString(string:"Title", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        addDescField.attributedPlaceholder = NSAttributedString(string:"Description", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        addLinkField.attributedPlaceholder = NSAttributedString(string:"IMDB Link", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor()])
        addMovieBioField.attributedText = NSAttributedString(string:"Plot", attributes:[NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName : UIFont(name: "Avenir Book", size: 14)!])
    }
    
    @IBAction func saveNewFavouriteMoviePressed(sender: AnyObject) {
        
        if let title = addTitleField.text where title != "" {
            
            let app =  UIApplication.sharedApplication().delegate as! AppDelegate
            let context  = app.managedObjectContext
            let entity = NSEntityDescription.entityForName("Movies", inManagedObjectContext: context)
            let movie = Movies(entity: entity!, insertIntoManagedObjectContext: context)
            movie.movieTitle = addTitleField.text
            movie.movieDesc = addDescField.text
            movie.movieLink = addLinkField.text
            movie.movieBio = addMovieBioField.text
            movie.saveFavMovieImg(movieImage.image!)
            
            context.insertObject(movie)
            
            do {
                try context.save()
                print("Favourite Movie was saved.")
            } catch  {
                print("A error occured. Favourite Movie could not be saved.")
            }
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    @IBAction func addNewMovieImage(sender: AnyObject) {
        presentViewController(imgPicker, animated: true, completion: nil)
    }
    
    
    @IBAction func cancelAddMovieBtnPressed(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
//        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imgPicker.dismissViewControllerAnimated(true, completion: nil)
        movieImage.image = image
        
    }
}