//
//  EditMovieVC.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/19/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit
import CoreData

class EditMovieVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var editTitleField: UITextField!
    @IBOutlet weak var editDescField: UITextField!
    @IBOutlet weak var editLinkField: UITextField!
    @IBOutlet weak var editMovieBioField: UITextView!
    @IBOutlet weak var editMovieImage: UIImageView!
    
    var movie: Movies!
    
    var mainMoc: NSManagedObjectContext?
    let batch = false
    
    
    var imgPicker: UIImagePickerController!
    
    var editMovTitle: String!
    var editMovDesc: String!
    var editMovLink: String!
    var editMovBio: String!
    var editMovImg: UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgPicker = UIImagePickerController()
        editMovieImage.layer.cornerRadius = editMovieImage.bounds.width / 2
        editMovieImage.clipsToBounds = true
        
        editTitleField.text = editMovTitle
        editDescField.text = editMovDesc
        editLinkField.text = editMovLink
        editMovieBioField.text = editMovBio
        editMovieImage.image = editMovImg
        
    }

    @IBAction func cancelEditMovieDetails(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    @IBAction func addNewMovieImage(sender: AnyObject) {
        presentViewController(imgPicker, animated: true, completion: nil)
    }
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        imgPicker.dismissViewControllerAnimated(true, completion: nil)
        editMovieImage.image = image
        
    }

}
