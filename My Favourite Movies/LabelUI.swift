//
//  LabelUI.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/18/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit

class LabelUI: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        self.layer.cornerRadius = 5.0;
        self.layer.borderColor = UIColor(red: 202 / 255, green: 202 / 255, blue: 202 / 255, alpha: 1.0).CGColor
        self.layer.borderWidth = 0.5
        self.tintColor = UIColor.whiteColor()
        self.backgroundColor = UIColor(red: 229 / 255, green: 228 / 255, blue: 229 / 255, alpha: 0.6)
        self.textColor = UIColor(red: 68 / 255, green: 68 / 255, blue: 68 / 255, alpha: 1.0)
        
    }
}