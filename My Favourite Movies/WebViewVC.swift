//
//  WebViewVC.swift
//  My Favourite Movies
//
//  Created by Daniel Esposito on 4/18/16.
//  Copyright © 2016 Daniel Esposito. All rights reserved.
//

import UIKit
import WebKit

class WebViewVC: UIViewController {
    
    @IBOutlet weak var webContainer: UIView!
    var webView: WKWebView!
    var movieLink: String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView = WKWebView()
        webContainer.addSubview(webView)
    }
    
    override func viewDidAppear(animated: Bool) {
        let frame = CGRectMake(0, 0, webContainer.bounds.width, webContainer.bounds.height)
        webView.frame = frame
        loadRequest(movieLink)
    }

    @IBAction func closeMovieLinkWebView(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion:nil)
    }
    
    func loadRequest(urlStr: String) {
        let url = NSURL(string: urlStr)!
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
    }
}
